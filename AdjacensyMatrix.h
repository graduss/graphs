#include "Graphs.h"

#ifndef ADJACENSYMATRIX_H
#define ADJACENSYMATRIX_H

namespace GRAPHS {

class AdjacensyMatrix : public Graphs {
    public:
        AdjacensyMatrix();
        AdjacensyMatrix(unsigned n);
        virtual ~AdjacensyMatrix();
        virtual AdjacensyMatrix &addEdje(unsigned n1, unsigned n2);
        virtual AdjacensyMatrix &removeEdje(unsigned n1, unsigned n2);
        virtual void output() const;
        virtual bool isAdjacent(unsigned n1, unsigned n2) const;
        virtual unsigned getNumVertices() const;
        virtual std::vector<unsigned> getAdjacent(unsigned n) const;
    private:
        unsigned num_vertices;
        bool **matrix;
};

}
#endif // ADJACENSYMATRIX_H
