#include "AdjacensyMatrix.h"

namespace GRAPHS {

std::vector<unsigned> AdjacensyMatrix::getAdjacent(unsigned n) const{
    if(n >= num_vertices) throw "for a range of values";
    std::vector<unsigned> answer;

    for(unsigned i = 0; i<num_vertices; i++){
        if(matrix[n][i]){
            answer.push_back(i);
        }
    }

    return answer;
}

unsigned AdjacensyMatrix::getNumVertices() const {
    return num_vertices;
}

AdjacensyMatrix::AdjacensyMatrix() {
    if(matrix){
        *matrix  = 0;
    }
    matrix = 0;
    num_vertices = 0;
}

AdjacensyMatrix::AdjacensyMatrix(unsigned n){
    num_vertices = n;
    if(n == 0) return;
    matrix = new bool*[num_vertices];
    for(unsigned i = 0; i<num_vertices; i++){
        matrix[i] = new bool[num_vertices];
    }
}

AdjacensyMatrix::~AdjacensyMatrix() {
    if(matrix){
        for(unsigned i=0; i<num_vertices; i++){
            if(matrix[i]){
                delete [] matrix[i];
                matrix[i] = 0;
            }
        }
        matrix = 0;
    }
}

AdjacensyMatrix &AdjacensyMatrix::addEdje(unsigned n1, unsigned n2){
    if(!isAdjacent(n1,n2)){
        matrix[n1][n2] = true;
        matrix[n2][n1] = true;
    }
    return *this;
}

AdjacensyMatrix &AdjacensyMatrix::removeEdje(unsigned n1, unsigned n2){
    if(isAdjacent(n1,n2)){
        matrix[n1][n2] = false;
        matrix[n2][n1] = false;
    }
    return *this;
}

bool AdjacensyMatrix::isAdjacent(unsigned n1, unsigned n2) const {
    if(n1 >= num_vertices || n2 >= num_vertices) throw "";
    else return matrix[n1][n2] && matrix[n2][n1];
}


void AdjacensyMatrix::output() const {
    if(matrix){
        std::cout<<"*** num vertices = "<<num_vertices<<" ***\n";
        for(unsigned i=0; i<num_vertices; i++){
            std::cout<<i<<" - ";
            for(unsigned q=0; q<num_vertices; q++){
                if(matrix[i][q]) std::cout<<q<<",";
            }
            std::cout<<"\n";
        }
        std::cout<<"***************\n";
    }else std::cout<<"*** empty ***\n";
}

}
