#include <list>
#include "Graphs.h"

#ifndef ADJACENCYLIST_H
#define ADJACENCYLIST_H

namespace GRAPHS {

class AdjacencyList : public Graphs
{
    public:
        AdjacencyList(): num_vertices(0), vertices(NULL) {};
        AdjacencyList(unsigned n);
        virtual ~AdjacencyList();
        virtual AdjacencyList &addEdje(unsigned n1, unsigned n2);
        virtual AdjacencyList &removeEdje(unsigned n1, unsigned n2);
        virtual void output() const;
        virtual bool isAdjacent(unsigned n1, unsigned n2) const;
        virtual unsigned getNumVertices() const;
        virtual std::vector<unsigned> getAdjacent(unsigned n) const;
    private:
        unsigned num_vertices;
        std::list<unsigned> *vertices;
};

}
#endif // AGJACENCYLIST_H
