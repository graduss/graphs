#include "Algorithms.h"

namespace GRAPHS {

Bfs *breadthFirstSearch(unsigned n, const Graphs *g){
    unsigned num_vertices = g->getNumVertices();
    std::queue<unsigned> Q;
    Bfs *b = new Bfs[num_vertices];

    b[n].color = Bfs::gray;
    Q.push(n);

    while (!Q.empty()){
        unsigned item = Q.front();
        std::vector<unsigned> list = g->getAdjacent(item);
        std::vector<unsigned>::iterator iter;
        for(iter = list.begin(); iter != list.end(); iter++){
            if(b[*iter].color == Bfs::white){
                b[*iter].color = Bfs::gray;
                b[*iter].parent = item;
                b[*iter].distance = b[item].distance + 1;
                Q.push(*iter);
            }
        }
        Q.pop();
        b[item].color = Bfs::black;
    }

    return b;
}

unsigned time;

void visit(unsigned n, Dfs *d, const Graphs *g){
    //time++;
    d[n].tBegin = ++time;
    d[n].color = Dfs::gray;
    std::vector<unsigned> list = g->getAdjacent(n);
    std::vector<unsigned>::iterator iter;
    for(iter = list.begin(); iter!=list.end(); iter++){
        if(d[*iter].color == Dfs::white){
            d[*iter].parent = n;
            visit(*iter, d, g);
        }
    }
    d[n].color = Dfs::black;
    d[n].tEnd = ++time;
}

Dfs *depthFirstSearch(const Graphs *g) {
    unsigned num_vertices = g->getNumVertices();
    Dfs *d = new Dfs[num_vertices];
    time = 0;

    for(unsigned i=0; i<num_vertices; i++){
        if(d[i].color == Dfs::white){
            visit(i, d, g);
        }
    }

    return d;
}

}
