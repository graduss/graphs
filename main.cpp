#include <iostream>
#include <cstdlib>
#include <vector>
#include "AdjacensyMatrix.h"
#include "AdjacencyList.h"
#include "IncidenceMatrix.h"
#include "Algorithms.h"

using namespace std;

int main()
{
    unsigned n = 20;
    //GRAPHS::AdjacensyMatrix g(n);
    GRAPHS::AdjacencyList g(n);
    //GRAPHS::IncidenceMatrix g(n);
    //g.addEdje(5,6);
    for(unsigned i = 0; i<30; i++){
        g.addEdje(rand()%n, rand()%n);
    }
    g.output();
    /*GRAPHS::Bfs *b = GRAPHS::breadthFirstSearch(2, &g);
    for(unsigned i = 0; i<n; i++){
        cout<<i<<" - parent: "<<b[i].parent<<" dist: "<<b[i].distance<<"\n";
    }*/
    GRAPHS::Dfs *d = GRAPHS::depthFirstSearch(&g);
    for(unsigned i = 0; i<n; i++){
        cout<<i<<" - parent: "<<d[i].parent<<" begin: "<<d[i].tBegin<<" end: "<<d[i].tEnd<<"\n";
    }
    /*g.output();

    g.addEdje(2,3).addEdje(4,5).addEdje(7,3);
    g.output();

    g.removeEdje(4,5);*/
    //g.output();

    return 0;
}
