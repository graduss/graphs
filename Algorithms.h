#include <queue>
#include "Graphs.h"

#ifndef ALGORITHMS_H_INCLUDED
#define ALGORITHMS_H_INCLUDED

namespace GRAPHS {

struct Bfs{
    enum Colors { black, white, gray } color;
    int parent;
    unsigned distance;
    Bfs() : color(white), parent(-1), distance(0) {}
};

Bfs *breadthFirstSearch(unsigned n, const Graphs *g);

struct Dfs {
    enum Color { black, white, gray } color;
    int parent;
    unsigned tBegin, tEnd;
    Dfs(): color(white), parent(-1), tBegin(0), tEnd(0) {}
};

Dfs *depthFirstSearch(const Graphs *g);

}

#endif // ALGORITHMS_H_INCLUDED
