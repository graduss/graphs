#include "Graphs.h"


#ifndef INCIDENCEMATRIX_H
#define INCIDENCEMATRIX_H

namespace GRAPHS{

class IncidenceMatrix : public Graphs{
    public:
        IncidenceMatrix(): num_vertices(0), matrix(NULL) {}
        IncidenceMatrix(unsigned n);
        virtual ~IncidenceMatrix();
        virtual IncidenceMatrix &addEdje(unsigned n1, unsigned n2);
        virtual IncidenceMatrix &removeEdje(unsigned n1, unsigned n2);
        virtual void output() const;
        virtual bool isAdjacent(unsigned n1, unsigned n2) const;
        virtual unsigned getNumVertices() const;
        virtual std::vector<unsigned> getAdjacent(unsigned n) const;
    private:
        unsigned num_vertices;
        std::vector<bool> *matrix;
};

}

#endif // INCIDENCEMATRIX_H
