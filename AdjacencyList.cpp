#include "AdjacencyList.h"

namespace GRAPHS {

std::vector<unsigned> AdjacencyList::getAdjacent(unsigned n) const {
    if(n >= num_vertices) throw "for a range of values";
    std::vector<unsigned> answer;

    std::list<unsigned>::iterator iter;
    for(iter = vertices[n].begin(); iter != vertices[n].end(); iter++){
        answer.push_back((*iter));
    }

    return answer;
}

unsigned AdjacencyList::getNumVertices() const {
    return num_vertices;
}

AdjacencyList& AdjacencyList::addEdje(unsigned n1, unsigned n2){
    if(!isAdjacent(n1,n2)) {
        vertices[n1].push_back(n2);
        if(n1 != n2) vertices[n2].push_back(n1);
    }

    return *this;
}

AdjacencyList& AdjacencyList::removeEdje(unsigned n1, unsigned n2){
    if(n1>=num_vertices || n2>=num_vertices) throw "for a range of values";

    std::list<unsigned>::iterator iter;
    for(iter = vertices[n1].begin(); iter != vertices[n1].end(); iter++){
        if(*iter == n2){
            vertices[n1].erase(iter);
            break;
        }
    }

    if(n1 == n2) return *this;

    for(iter = vertices[n2].begin(); iter != vertices[n2].end(); iter++){
        if(*iter == n1){
            vertices[n2].erase(iter);
            break;
        }
    }

    return *this;
}

bool AdjacencyList::isAdjacent(unsigned n1, unsigned n2) const{
    if(n1>=num_vertices || n2>=num_vertices) throw "for a range of values";

    bool answer = false;

    std::list<unsigned>::iterator iter;
    for(iter = vertices[n1].begin(); iter != vertices[n1].end(); iter++){
        if(*iter == n2) {
            answer = true;
            break;
        }
    }

    return answer;
}

void AdjacencyList::output() const {
    if(vertices){
        std::cout<<"*** num vertices = "<<num_vertices<<" ***\n";
        std::list<unsigned>::iterator iter;
        for(unsigned i = 0; i<num_vertices; i++){
            std::cout<<i<<" - ";
            for(iter = vertices[i].begin(); iter != vertices[i].end(); iter++){
                std::cout<<*iter<<",";
            }
            std::cout<<"\n";
        }
        std::cout<<"***************\n";
    }else std::cout<<"*** empty ***\n";
}

AdjacencyList::AdjacencyList(unsigned n){
    vertices = new std::list<unsigned>[n];
    if(vertices){
        num_vertices = n;
    } else throw "Mem ERROR";
}

AdjacencyList::~AdjacencyList(){
    if(vertices){
        delete [] vertices;
        num_vertices = 0;
    }
}

}
